#include <iostream>



bool consume(char c) {
  if(std::cin.peek() == c) {
    std::cin.get();
    return true;
  }
  return false;
}

bool term();

bool seq() {
  if(!term()) return false;
  if(consume('_')) {
    return seq();
  }
  return true;
}

bool term() {
  if(consume('A')) {
    while(consume('a'));
    return true;
  }
  if(consume('a')) {
    while(consume('a'));
    return true;
  }
  return false;
}

int main () {

  if(seq()) {
    std::cout << "Yes\n";
  }else{
    std::cout << "No\n";
  }

  return 0;

}
