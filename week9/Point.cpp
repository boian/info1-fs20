#include <iostream>

class Point {
public:
  int x;
  int y;
  Point(int x_, int y_);
  Point operator+(const Point other);
};

Point::Point(int x_, int y_) {
  x = x_;
  y = y_;
}

Point Point::operator+(const Point other) {
  return Point(x + other.x, y + other.y);
}

std::istream& operator>>(std::istream &in, Point& p) {
  std::cin >> p.x >> p.y;
  return in;
}

std::ostream& operator<<(std::ostream &out, const Point& p) {
  std::cout << "(" << p.x << " " << p.y << ")";
  return out;
}

int main () {
  Point a(5, 0);
  Point b(0,0);
  std::cin >> b;
  std::cout << (a+b) << std::endl;
}
